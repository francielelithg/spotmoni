import { createStore, applyMiddleware } from 'redux'
import { createWrapper, HYDRATE } from 'next-redux-wrapper'
import thunkMiddleware from 'redux-thunk'

const initialState = {
  languages: [],
  codes: [],
  translations: []
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case HYDRATE:
      return { 
        ...state, 
        languages: action.payload.languages,
        codes: action.payload.codes,
        translations: action.payload.translations
      }
    case 'TICK':
      return { 
        ...state, 
        languages: action.languages,
        codes: action.codes,
        translations: action.translations
      }
    default:
      return state
  }
}

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension')
    return composeWithDevTools(applyMiddleware(...middleware))
  }
  return applyMiddleware(...middleware)
}

// eslint-disable-next-line no-unused-vars
const makeStore = context => createStore(reducer, bindMiddleware([thunkMiddleware]))

export const wrapper = createWrapper(makeStore, {debug: true})