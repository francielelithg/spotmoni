import { useCallback, useContext, useState } from 'react'
import { useRouter } from 'next/router'
import { LocaleContext } from '../context/LocaleContext'
import {
  Backdrop,
  Box,
  CircularProgress,
  Select,
  MenuItem
} from '@material-ui/core'
import ExpandMore from '@material-ui/icons/ExpandMore'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import React from 'react'

const LocaleSwitcher = ({ languages, codes }) => {
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const { locale } = useContext(LocaleContext)
  const classes = useStyles()

  const handleLocaleChange = useCallback((e) => {
    setOpen(true)
    const regex = new RegExp(`(${codes.join('|')})`)
    router.push(router.pathname, router.asPath.replace(regex, `${e.target.value}`))
    setTimeout(() => {
      setOpen(false)
    }, 500)
  }, [router]);

  return (
    <Box ml={2}>
      <Select
        value={locale}
        classes={{ root: classes.root, icon: classes.icon }}
        onChange={handleLocaleChange}
        IconComponent={ExpandMore}
        disableUnderline>
        {languages && languages.map((language, index) => (
          <MenuItem key={index} value={language.code}>
            {language.name}
          </MenuItem>
        ))}
      </Select>

      <Backdrop className={classes.backdrop} open={open}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '6px',
    color: theme.palette.secondary.main,
    fontSize: theme.typography.pxToRem(20),
  },
  icon: {
    color: theme.palette.secondary.main,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: theme.palette.secondary.main,
  },
}))


export default connect((state) => state)(LocaleSwitcher)