import { getSession, signOut, useSession } from 'next-auth/client'
import React, { useState } from 'react'

const Login = () => {
  return (<></>)
}

export async function getServerSideProps(context) {
  const session = await getSession(context)
  return {
    props: { session }
  }
}

export default Login