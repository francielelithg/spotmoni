import {
  Box,
  Card,
  Container,
  Grid,
  Typography
} from '@material-ui/core'
import { findFlagUrlByIso2Code } from "country-flags-svg"
import { makeStyles } from '@material-ui/core/styles'
import React from 'react'

const us = findFlagUrlByIso2Code('GB')

const Countries = () => {
  const classes = useStyles()
  return (
    <Container maxWidth='lg'>
      <Box pt={12} pb={18}>
        <Typography
          component='h4'
          variant='h5'
          align='center'
          color="primary">
          Find the best way to send money to each country. Filtered by country.
          </Typography>
        <Box mt={6}>
          <Container maxWidth='md'>
            <Grid container spacing={5}>
              <Grid item xs={12} sm={4}>
                <Card classes={{ root: classes.purple }}>
                  <Box p={4}>
                    <Box mb={2}>
                      <Typography variant='h5' color="secondary">
                        Popular countries
                    </Typography>
                    </Box>
                    <Card classes={{ root: classes.root }} />
                    <Card classes={{ root: classes.root }} />
                    <Card classes={{ root: classes.root }} />
                  </Box>
                </Card>
              </Grid>
              <Grid item xs={12} sm={8}>
              <Box p={4}>
              <Box mb={2}>
                <Typography variant='h5'>
                  All countries
                    </Typography>
                    </Box>
                <Grid container spacing={5}>
                  <Grid item xs={12} sm={6}>
                    <Card classes={{ root: classes.root }} />
                    <Card classes={{ root: classes.root }} />
                    <Card classes={{ root: classes.root }} />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Card classes={{ root: classes.root }} />
                    <Card classes={{ root: classes.root }} />
                    <Card classes={{ root: classes.root }} />
                  </Grid>
                </Grid>
                </Box>
              </Grid>
            </Grid>
          </Container>
        </Box>
      </Box>
    </Container>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    // margin: theme.spacing(0, 4, 0, 0),
    width: '54px',
    height: '54px',
    borderRadius: '27px',
    backgroundImage: `url(${us})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    marginTop: '20px',
    marginBottom: '20px'
  },
  purple: {
    backgroundColor: theme.palette.primary.main
  }
}))

export default Countries