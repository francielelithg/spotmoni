import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Button,
  Container,
  Grid,
  Typography
} from '@material-ui/core'
import React from 'react'

const RelatedCountryText = () => {
  const classes = useStyles()

  return (
    <div className={classes.blog}>
      <Container maxWidth='md'>
        <Box pt={22} pb={12}>
          <Box mb={4}>
            <Typography
              component='h4'
              variant='h5'
              align='center'
              color="primary">
              How to send money from India to the United States at the best rates?
            </Typography>
          </Box>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={6} className={classes.left}>
              <Typography variant='h6'>
                If everyone knows about and uses the most inexpensive option for their money transfer, $28 billion in execssive transfer fees and forex rates could be saved each year. If everyone knows about and uses the most inexpensive option for their money transfer, $28 billion in execssive transfer fees and forex rates could be saved each
                </Typography>
            </Grid>
            <Grid item xs={12} sm={6} className={classes.right}>
              <Typography variant='h6'>
                year. If everyone knows about and uses the most inexpensive option for their money transfer, $28 billion in execssive transfer fees and forex rates could be saved each year. If everyone knows about and uses the most inexpensive option for their money transfer, $28 billion…
                </Typography>
            </Grid>
          </Grid>
        </Box>
        <Box justifyContent="center" display="flex">
          <Button
            size='large'
            color='secondary'
            variant='contained'
            type='submit'
            classes={{ root: classes.button, label: classes.label }}>
            Read more
          </Button>
        </Box>
      </Container>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  blog: {
    backgroundImage: `url(${"/images/backgroundDivider.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'top',
    backgroundRepeat: 'no-repeat',
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  button: {
    marginBottom: '-20px',
    position: 'relative',
  },
  label: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
    textTransform: 'none'
  },
  left: {
    [theme.breakpoints.up('sm')]: {
      paddingRight: theme.spacing(2),
    },
  },
  right: {
    [theme.breakpoints.up('sm')]: {
      pddingLeft: theme.spacing(2),
    },
  }
}))

export default RelatedCountryText