import { useRouter } from 'next/router'
import { getSession, signOut, useSession } from 'next-auth/client'
import LocaleSwitcher from './LocaleSwitcher'
import {
  Avatar,
  Box,
  Button,
  Divider,
  IconButton,
  Link,
  Menu,
  MenuItem,
  Container,
  Typography
} from '@material-ui/core'
import { MoreVert } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import useTranslation from '../hooks/useTanslation'
import { connect } from 'react-redux'
import React, { useState, useEffect } from 'react'

const MenuHeader = ({ translations }) => {
  const classes = useStyles()
  const [session] = useSession()
  const { locale } = useTranslation()
  const [menu, setMenu] = useState(null)
  const [mobileMoreAnchor, setMobileMoreAnchor] = useState(null)
  const [options, setOptions] = useState()
  const isMobileMenuOpen = Boolean(mobileMoreAnchor)
  const router = useRouter()

  useEffect(() => {
    if (translations) {
      setOptions(translations.filter(value => value.postType == 'menu'))
    }
  }, [locale])

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchor(event.currentTarget)
  }

  const handleMobileMenuClose = () => {
    setMobileMoreAnchor(null)
  }

  const handleOpen = event => {
    setMenu(event.currentTarget)
  }

  const handleClose = () => {
    setMenu(null)
  }

  const handleLogin = () => {
    router.push(`/auth/signin`)
  }

  const handleMenuClick = link => {
    router.push(`/[lang]${link}`, `/${router.query.lang}${link}`)
  }

  return (
    <Container maxWidth='lg'>
      <Box py={8} display='flex' fontWeight='fontWeightMedium' fontSize={20} alignItems="center">
        <Box flexGrow={1}>
          <Box display="flex" flexDirection="row">
            <div className={classes.sectionDesktop}>
              <Box display="flex" flexDirection="row" alignItems="center">
                {options && options.map((option, index) => (
                  <Link onClick={() => handleMenuClick(option.link)} underline="none" key={index} className={classes.link}>
                    <Typography variant='h6' color="secondary">{JSON.parse(option.content).name}</Typography>
                  </Link>
                ))}
                <Divider orientation="vertical" flexItem classes={{ root: classes.divider }} />
              </Box>
            </div>

            <div className={classes.sectionMobile}>
              <IconButton
                color="secondary"
                onClick={handleMobileMenuOpen}>
                <MoreVert />
              </IconButton>

              <Menu
                anchorEl={mobileMoreAnchor}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                keepMounted
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMobileMenuOpen}
                onClose={handleMobileMenuClose}
              >
                {options && options.map((option, index) => (
                  <MenuItem key={index} onClick={() => handleMenuClick(option.link)}>
                    {JSON.parse(option.content).name}
                  </MenuItem>
                ))}
              </Menu>
            </div>
                
            <Box display="flex" alignItems="center">
              <LocaleSwitcher />
            </Box>
          </Box>
        </Box>
        <Box>
          {!session && (
            <Button
              size='large'
              color='secondary'
              variant='contained'
              classes={{ label: classes.label }}
              onClick={handleLogin}
            >Sign Up/In</Button>
          )}
          {session && (
            <>
              <Link href="/profile">
                <a>
                </a>
              </Link>
              <Avatar src={session.user.image} aria-controls="menu-user" aria-haspopup="true" onClick={handleOpen} />
              <Menu
                id="menu-user"
                anchorEl={menu}
                keepMounted
                open={Boolean(menu)}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose}>
                  <Link href="/[lang]/account" as={`/${locale}/account`}><a>MY ACCOUNT</a></Link><br />
                </MenuItem>
                <MenuItem onClick={handleClose}>
                  <a onClick={(e) => {
                    e.preventDefault()
                    signOut()
                  }}
                  >SIGN OUT</a>
                </MenuItem>
              </Menu>
            </>
          )}
        </Box>
      </Box>
    </Container>
  )
}

const useStyles = makeStyles((theme) => ({
  link: {
    margin: theme.spacing(0, 6, 0, 0),
    color: theme.palette.secondary.main,
    '&:hover': {
      cursor: 'pointer'
    },
  },
  divider: {
    backgroundColor: theme.palette.secondary.main,
  },
  label: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
    textTransform: 'none'
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}))

export async function getServerSideProps(context) {
  const session = await getSession(context)
  return {
    props: { session }
  }
}

export default connect((state) => state)(MenuHeader)