import { makeStyles } from '@material-ui/core/styles'
import { useState } from 'react'
import {
  Box,
  Button,
  Chip,
  Container,
  Grid,
  Paper,
  Typography
} from '@material-ui/core'
import {
  OpenInNew
} from '@material-ui/icons'
import React from 'react'
import { useRouter } from 'next/router'

const ResultCard = (props) => {
  const classes = useStyles()
  const router = useRouter()

  const openProvider = () => {
    router.push(`/[lang]/provider`, `/${router.query.lang}/provider`)
  }

  return (
    <div>
      <Box my={4}>
        <Container maxWidth='md'>
          <Paper elevation={0} className={classes.body}>
            <Grid container spacing={0}>
              <Grid item xs={12} md={4} className={classes.provider}>
                <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                  <img src='/images/worldremit.svg' />
                </Box>
              </Grid>
              <Grid item xs={12} md={4}>
                <Box mx={3} mt={3} mb={2}>
                  <Chip label="FEATURED" className={classes.tag} size='small' />
                  <Typography variant='subtitle1'>
                    <Box fontWeight="fontWeightBold" mt={1} css={{ color: '#aaa' }}>
                      {props.input} {props.from} =
                    </Box>
                </Typography>
                  <Typography variant='h4'>
                    <Box fontWeight="fontWeightBold">
                      {props.output} {props.to}
                    </Box>
                  </Typography>
                </Box>
              </Grid>
              <Grid item xs={12} md={4}>
                <Box display="flex" alignItems="center" css={{ height: '100%' }} mx={3}>
                  <Button
                    fullWidth
                    size='large'
                    color='secondary'
                    variant='contained'
                    type='submit'
                    endIcon={<OpenInNew />}
                    classes={{ root: classes.button, label: classes.label }}>
                    Use {props.company}
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Paper>
        </Container>
      </Box>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  body: {
    background: theme.palette.secondary.light,
  },
  tag: {
    color: theme.palette.secondary.main,
    fontWeight: 'bold',
    background: theme.palette.featured.main,
  },
  button: {
    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(3),
    },
  },
  label: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
    textTransform: 'none'
  },
  provider: {
    background: theme.palette.secondary.dark,
    borderTopLeftRadius: '3px',
    borderBottomLeftRadius: '3px',
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(2, 0),
    },
  },
}))

export default ResultCard