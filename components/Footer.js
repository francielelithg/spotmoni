import Link from 'next/link'
import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  CardMedia,
  Grid,
  Container,
  Typography
} from '@material-ui/core'
import Logo from './Logo'
import useTranslation from '../hooks/useTanslation'
import React from 'react'

const Footer = () => {
  const classes = useStyles()
  const { locale } = useTranslation()

  return (
    <footer className={classes.footerBody}>
      <Container maxWidth='lg'>
        <Box pt={24} pb={18}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={5}>
              <Logo />
              <Typography variant="body2" gutterBottom className={classes.copyright}>
                Copyright© 2020.
              </Typography>
            </Grid>
            <Grid item xs={12} md={2}>
              <Typography variant="body2" gutterBottom className={classes.footerText}>
                <Link href="/[lang]" as={`/${locale}`}><a>Home</a></Link><br />
                <Link href="/[lang]/blog" as={`/${locale}/blog`}><a>Blog</a></Link><br />
                FAQs<br />
                Sitemap<br />
                Wiki<br />
              </Typography>
            </Grid>
            <Grid item xs={12} md={2}>
              <Typography variant="body2" gutterBottom className={classes.footerText}>
              <Link href="/[lang]/terms-of-service" as={`/${locale}/terms-of-service`}><a>Terms of service</a></Link><br />
                <Link href="/[lang]/privacy-policy" as={`/${locale}/privacy-policy`}><a>Privacy policy</a></Link><br />
                <Link href="/[lang]/about" as={`/${locale}/about`}><a>About</a></Link><br />
              </Typography>
            </Grid>
            <Grid item xs={12} md={3}>
              <Typography variant="body2" gutterBottom className={classes.footerText}>
                help@spotmoni.com
              </Typography>
              <Box classes={{ root: classes.social }}>
                <CardMedia
                  component="img"
                  alt="Twitter"
                  height="140"
                  image="/images/social/facebook.png"
                  classes={{ root: classes.cardRoot }}
                />
                <CardMedia
                  component="img"
                  alt="Twitter"
                  height="140"
                  image="/images/social/twitter.png"
                  classes={{ root: classes.cardRoot }}
                />
                <CardMedia
                  component="img"
                  alt="Twitter"
                  height="140"
                  image="/images/social/instagram.png"
                  classes={{ root: classes.cardRoot }}
                />
                <CardMedia
                  component="img"
                  alt="Twitter"
                  height="140"
                  image="/images/social/copylink.png"
                  classes={{ root: classes.cardRoot }}
                />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </footer>
  )
}

const useStyles = makeStyles((theme) => ({
  footerBody: {
    width: '100%',
    backgroundImage: `url(${"/images/backgroundFooter.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'top',
    backgroundRepeat: 'no-repeat'
  },
  footerText: {
    fontSize: theme.typography.pxToRem(18),
    color: theme.palette.secondary.main,
    [theme.breakpoints.up('md')]: {
      textAlign: 'right',
    }
  },
  social: {
    marginTop: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      float: 'right',
    }
  },
  cardRoot: {
    display: 'inline',
    verticalAlign: 'right',
    height: '26px',
    width: '26px',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    [theme.breakpoints.down('md')]: {
      marginRight: theme.spacing(2),
    },
    [theme.breakpoints.up('md')]: {
      marginLeft: theme.spacing(3),
    }
  },
  copyright: {
    fontSize: theme.typography.pxToRem(18),
    color: theme.palette.secondary.dark,
  }
}))

export default Footer