import { useRouter } from 'next/router'
import {
  Box,
  Card,
  CardMedia,
  InputBase,
  Button,
  IconButton,
  Paper,
  MenuItem,
  ListItemIcon,
  Select,
  Grid,
  Typography
} from '@material-ui/core'
import { SwapHoriz, SwapVert, ExpandMore } from '@material-ui/icons'
import { findFlagUrlByIso2Code } from "country-flags-svg"
import { makeStyles } from '@material-ui/core/styles'
import countryService from '../services/country'
import useForm from '../hooks/useForm'
import React from 'react'
import {useState, useEffect} from 'react'

const CompareForm = props => {
  const classes = useStyles()
  const [countries, setCountries] = useState()

  useEffect(() => {
    async function fetchData() {
      const data = await countryService.getAll()
      data.sort((a, b) => {
        if(a.currencyCode < b.currencyCode) { return -1 }
        if(a.currencyCode > b.currencyCode) { return 1 }
        return 0
      })
      setCountries(data)
    }
    fetchData()
  }, []);

  const { values, handleChange, handleSubmit } = useForm({
    from: props.from || '',
    to: props.to || '',
    amount: props.amount || '',
  }, redirect)

  const router = useRouter()

  function redirect() {
    router.push(`/[lang]/compare/[from]/[to]/[amount]`,
      `/${router.query.lang}/compare/${values.from}/${values.to}/${values.amount}`)
  }

  const menuProps = {
    anchorOrigin: {
      vertical: "bottom",
      horizontal: "left",
    },
    transformOrigin: {
      vertical: "top",
      horizontal: "left"
    },
    getContentAnchorEl: null,
  };

  return (
    <Box py={3}>
      <Box mb={2} />
      <form onSubmit={handleSubmit}>
        <Grid container spacing={0}>
          <Grid item xs={12} md={4}>
            <Paper elevation={1}>
              <Grid container spacing={0}>
                <Grid item xs={7}>
                  <Box p={2}>
                    <Typography className={classes.label}>FROM COUNTRY</Typography>
                    <InputBase
                      classes={{ input: classes.value }}
                      value={values.amount}
                      name="amount"
                      onChange={handleChange}
                      type="number"
                      placeholder="0.00"
                    />
                  </Box>
                </Grid>
                <Grid item xs={5} className={classes.countrySelect}>
                  <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                    <Select
                      disableUnderline
                      IconComponent={ExpandMore}
                      MenuProps={menuProps}
                      classes={{ root: classes.select, icon: classes.icon }}
                      value={values.from}
                      name='from'
                      onChange={handleChange}
                    >
                      {countries && countries.map((country) => (
                        <MenuItem value={country.alpha3Code} key={country.countryId}>
                          <ListItemIcon>
                            <CardMedia classes={{ root: classes.root }} image={findFlagUrlByIso2Code(country.alpha2Code)} />
                          </ListItemIcon>
                          <Box pl={1}>{country.currencyCode}</Box>
                        </MenuItem>
                      ))}
                    </Select>
                  </Box>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={1}>
            <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
              <IconButton
                size='small'
                color='secondary'
                classes={{ root: classes.swapButton }}>
                <div className={classes.sectionDesktop}><SwapHoriz /></div>
                <div className={classes.sectionMobile}><SwapVert /></div>
              </IconButton>
            </Box>
          </Grid>
          <Grid item xs={12} md={4}>
            <Paper elevation={1}>
              <Grid container spacing={0}>
                <Grid item xs={7}>
                  <Box p={2}>
                    <Typography className={classes.label}>TO COUNTRY</Typography>
                    <InputBase
                      classes={{ input: classes.value }}
                      placeholder="0.00"
                      disabled
                    />
                  </Box>
                </Grid>
                <Grid item xs={5} className={classes.countrySelect}>
                  <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                    <Select
                      disableUnderline
                      IconComponent={ExpandMore}
                      MenuProps={menuProps}
                      classes={{ root: classes.select, icon: classes.icon }}
                      value={values.to}
                      name='to'
                      onChange={handleChange}
                    >
                      {countries && countries.map((country) => (
                        <MenuItem value={country.alpha3Code} key={country.countryId}>
                          <ListItemIcon>
                            <CardMedia classes={{ root: classes.root }} image={findFlagUrlByIso2Code(country.alpha2Code)} />
                          </ListItemIcon>
                          <Box pl={1}>{country.currencyCode}</Box>
                        </MenuItem>
                      ))}
                    </Select>
                  </Box>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={3}>
            <Box display="flex" alignItems="center" css={{ height: '100%' }}>
              <Button
                fullWidth
                size='large'
                color='secondary'
                variant='contained'
                type='submit'
                classes={{ root: classes.gridButton, label: classes.button }}>
                Compare
              </Button>
            </Box>
          </Grid>
        </Grid>
      </form >
    </Box >
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '54px',
    height: '54px',
    borderRadius: '27px',
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  select: {
    display: 'flex',
    alignItems: 'center',
    verticalAlign: 'center',
    color: theme.palette.secondary.main,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
  },
  icon: {
    color: theme.palette.secondary.main,
  },
  button: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
    textTransform: 'none',
  },
  gridButton: {
    [theme.breakpoints.up('sm')]: {
      margin: theme.spacing(0, 0, 0, 5),
    },
    [theme.breakpoints.down('sm')]: {
      margin: theme.spacing(3, 0, 0, 0),
    },
  },
  label: {
    fontWeight: 'bold'
  },
  swapButton: {
    background: '#3a3e60',
    boxShadow: '0px 3px 1px -2px rgba(0, 0, 0, 0.2)',
    '&:hover': {
      background: '#3a3e60',
    },
    [theme.breakpoints.down('md')]: {
      margin: theme.spacing(3, 0),
    },
  },
  value: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(30),
    padding: 0,
    fontWeight: 'bold',
  },
  countrySelect: {
    backgroundColor: '#3a3e60',
    borderTopRightRadius: '3px',
    borderBottomRightRadius: '3px',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}))

export default CompareForm