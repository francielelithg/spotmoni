import { Typography } from '@material-ui/core'
import React from 'react'

const Logo = () => {
  return (
    <Typography variant='h3' color='secondary'>
      SPOTMONI
    </Typography>
  )
}

export default Logo