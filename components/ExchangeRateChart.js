import {
  ArgumentAxis,
  ValueAxis,
  Chart,
  LineSeries,
} from '@devexpress/dx-react-chart-material-ui'
import React from 'react'

const data = [
  { argument: 1, value: 10 },
  { argument: 2, value: 15 },
  { argument: 3, value: 30 },
  { argument: 4, value: 28 },
  { argument: 5, value: 17 },
  { argument: 6, value: 15 },
  { argument: 7, value: 7 },
  { argument: 8, value: 13 },
  { argument: 9, value: 9 },
  { argument: 10, value: 25 },
  { argument: 11, value: 27 },
  { argument: 12, value: 22 }
]

const ExchangeRateChart = () => {
  return (
    <Chart data={data} height={150}>
      <ArgumentAxis />
      <ValueAxis />
      <LineSeries valueField="value" argumentField="argument" />
    </Chart>
  )
}

export default ExchangeRateChart