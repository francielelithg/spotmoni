import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Container,
  Grid,
  Typography
}
  from '@material-ui/core'
import useTranslation from '../hooks/useTanslation'
import React from 'react'

const HeadTitle = () => {
  const { t } = useTranslation()
  const classes = useStyles()

  return (
    <Container className={classes.titleBody}>
      <Grid container spacing={0}>
        <Grid item xs={12} md={6}>
          <Typography variant='h2'>
            SPOTMONI
          </Typography>
          <Typography variant='h5'>
            Compare the best rates before sending money abroad.
          </Typography>
        </Grid>
        <Grid item xs={12} md={6}>
          <Box pt={2}>
            <Grid container spacing={0}>
              <Grid item xs={4}>
                <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                  <Box mb={1}><img src="/images/providers.png" width="40px" height="38px" /></Box>
                </Box>
              </Grid>
              <Grid item xs={4}>
                <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                  <Box mb={1}><img src="/images/best.png" width="40px" height="38px" /></Box>
                </Box>
              </Grid>
              <Grid item xs={4}>
                <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                  <Box mb={1}><img src="/images/notified.png" width="40px" height="38px" /></Box>
                </Box>
              </Grid>
              <Grid item xs={4}>
                <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                  <Typography variant='subtitle2'>Trusted providers</Typography>
                </Box>
              </Grid>
              <Grid item xs={4}>
                <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                  <Typography variant='subtitle2'>Best rates</Typography>
                </Box>
              </Grid>
              <Grid item xs={4}>
                <Box display="flex" alignItems="center" justifyContent="center" css={{ height: '100%' }}>
                  <Typography variant='subtitle2'>Price drops alerts</Typography>
                </Box>
              </Grid>
            </Grid>
          </Box>

        </Grid>
      </Grid>
    </Container>
  )
}

const useStyles = makeStyles((theme) => ({
  titleBody: {
    color: '#fff',
    margin: theme.spacing(9, 0, 12, 0),
    padding: theme.spacing(0)
  }
}))

export default HeadTitle