import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Button,
  Container,
  Grid,
  Typography
} from '@material-ui/core'
import React from 'react'

const AdvantageCard = () => {
  const classes = useStyles()

  return (
    <>
      <Container maxWidth='lg'>
        <Box pt={12} pb={8}>
          <Box pb={4} className={classes.boxTitle}>
            <Typography variant='h5' color='primary'>
              We compare across 200+ global money transfer providers to show you the best rates
            </Typography>
          </Box>
          <Grid container spacing={0} className={classes.gridImages}>
            <Grid item xs={12} md={3}>
              <Box pr={2} py={2}>
                <img src='/images/providers/worldremit.jpg' height="40" />
              </Box>
            </Grid>
            <Grid item xs={12} md={3}>
              <Box pr={2} py={2}>
                <img src='/images/providers/transferwise.jpg' height="30" />
              </Box>
            </Grid>
            <Grid item xs={12} md={3}>
              <Box pr={2} py={2}>
                <img src='/images/providers/remitbee.jpg' height="40" />
              </Box>
            </Grid>
            <Grid item xs={12} md={3}>
              <Box pr={2} py={2}>
                <img src='/images/providers/remitly.jpg' height="40" />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>

      <div className={classes.divider}>
        <Container maxWidth='lg'>
          <Box pt={20} pb={8}>
            <Box pt={2} pb={4}>
              <Typography variant='h5' color='primary' align='center'>
                So why compare different services with Spotmoni.com?
                </Typography>
            </Box>
            <Box pt={2} pb={4}>
              <Container maxWidth='md'>
                <Typography variant='h5' align='center'>
                  If everyone knows about and uses the most inexpensive option for their money transfer, $28 billion in execssive transfer fees and forex rates could be saved each year.
                </Typography>
              </Container>
            </Box>
          </Box>
          <Box justifyContent="center" display="flex">
            <Button
              size='large'
              color='secondary'
              variant='contained'
              type='submit'
              classes={{ root: classes.button, label: classes.labelSignUp }}>
              Sign Up
            </Button>
            <Button
              size='large'
              color='primary'
              variant='contained'
              type='submit'
              classes={{ root: classes.button, label: classes.labelCompare }}>
              Compare Rates!
            </Button>
        </Box>
        </Container>
      </div>
    </>
  )
}

const useStyles = makeStyles((theme) => ({
  boxTitle: {
    [theme.breakpoints.up('md')]: {
      width: '50%',
    }
  },
  gridImages: {
    [theme.breakpoints.up('md')]: {
      width: '70%',
    }
  },
  divider: {
    backgroundImage: `url(${"/images/backgroundDivider.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'top',
    backgroundRepeat: 'no-repeat',
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  button: {
    marginBottom: '-20px',
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    position: 'relative',
  },
  labelSignUp: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
    textTransform: 'none'
  },
  labelCompare: {
    color: theme.palette.primary.secondary,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
    textTransform: 'none'
  },
}))

export default AdvantageCard