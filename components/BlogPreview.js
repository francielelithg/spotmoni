import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Button,
  Container,
  Grid,
  Typography
} from '@material-ui/core'
import Skeleton from '@material-ui/lab/Skeleton'
import React from 'react'

const BlogPreview = () => {
  const classes = useStyles()

  return (
    <div className={classes.blog}>
      <Container maxWidth='lg'>
        <Box pt={24} pb={12}>
          <Typography
            component='h4'
            variant='h5'
            align='center'
            color="primary">
            Read how others saved $$$ with Spotmoni
          </Typography>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={4}>
              <Box mx={3}>
                <Box my={4}>
                  <Skeleton variant="rect" width='100%' height="476px" />
                </Box>
                <Typography variant='h6'>
                  How to Minimize Your Forex Fees
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box mx={3}>
                <Box my={4}>
                  <Skeleton variant="rect" width='100%' height="476px" />
                </Box>
                <Typography variant='h6'>
                  Spotmoni.com’s Price Alert Feature Can Save You a Ton
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box mx={3}>
                <Box my={4}>
                  <Skeleton variant="rect" width='100%' height="476px" />
                </Box>
                <Typography variant='h6'>
                  How to Send Cheaper and Faster
                </Typography>
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Box justifyContent="center" display="flex">
          <Button
            size='large'
            color='secondary'
            variant='contained'
            type='submit'
            classes={{ root: classes.button, label: classes.label }}>
            Read more
          </Button>
        </Box>
      </Container>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  blog: {
    backgroundImage: `url(${"/images/backgroundBlog.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'top',
    backgroundRepeat: 'no-repeat',
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  button: {
    marginBottom: '-20px',
    position: 'relative',
  },
  label: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(18),
    fontWeight: 'bold',
    textTransform: 'none'
  },
}))

export default BlogPreview