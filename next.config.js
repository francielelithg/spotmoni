const withCSS = require('@zeit/next-css')

module.exports = withCSS({
  webpack: function (config) {
    config.module.rules.push({
      test: /\.(woff|ttf|svg|png|jpg|gif)$/,
      use: {
        loader: 'file-loader'
      }
    })
    return config
  }
})