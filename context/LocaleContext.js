import React, { useState, useEffect } from 'react'

export const LocaleContext = React.createContext({
  locale: 'en',
  setLocale: () => null
})

export const LocaleProvider = ({ lang, children }) => {
  const [locale, setLocale] = useState(lang)

  useEffect(() => {
    if (locale !== localStorage.getItem('locale')) {
      localStorage.setItem('locale', locale);
    }
  }, [locale])

  useEffect(() => {
    if (typeof lang === 'string' && locale !== lang) {
      setLocale(lang);
    }
  }, [lang, locale])

  return (<LocaleContext.Provider value={{ locale, setLocale }}>{children}</LocaleContext.Provider>)
}