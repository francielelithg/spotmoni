import { getDisplayName } from 'next/dist/next-server/lib/utils'
import { LocaleProvider } from '../context/LocaleContext'
import translationService from '../services/translation'
import React from 'react'

export default function withLocale(WrappedPage) {
  const WithLocale = (props) => {
    let locale = props.locale

    return (
      <LocaleProvider lang={locale}>
        <WrappedPage {...props} />
      </LocaleProvider>
    )
  }

  WithLocale.getInitialProps = async (ctx) => {
    let pageProps = {}

    let state = ctx.store.getState()

    if (WrappedPage.getInitialProps) {
      pageProps = await WrappedPage.getInitialProps(ctx)
    }

    if (typeof ctx.query.lang !== 'string' || !state.codes.some(code => code === ctx.query.lang)) {
      if (ctx.res) {
        ctx.res.writeHead(301, {
          Location: '/en'
        })
        ctx.res.end()
      }
    }

    ctx.store.dispatch({
      type: 'TICK',
      ...state,
      translations: await translationService.getPostsByLanguage(ctx.query.lang)
    })
    
    return Object.assign(Object.assign({}, pageProps), { locale: ctx.query.lang })
  }

  WithLocale.displayName = `withLang(${getDisplayName(WrappedPage)})`

  return WithLocale
}