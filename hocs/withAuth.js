import { useSession, getSession } from 'next-auth/client'
import React from 'react'

export default function withAuth(WrappedPage) {
  const WithAuth = props => {
    const [ session ] = useSession()

    if (!session) return <p>Access Denied</p>
    
    return (
      <WrappedPage {...props} />
    )
  }

  /*
  async function getServerSideProps(context) {
    const session = await getSession(context)
    return {
      props: { session }
    }
  }
  */

  return WithAuth
}