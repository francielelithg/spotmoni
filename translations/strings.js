const strings = {
  en: {
    slogan: 'Compare services before sending money abroad. Fast, safe and reliable.',
    home: 'HOME',
    about: 'ABOUT',
    youWantToSendMoney: 'You want to send money...',
    from: 'From',
    to: 'To',
    amount: 'Amount',
    compare: 'Compare',
    whyToCompare: 'Why to compare before sending money abroad?',
    getTheBestFees: 'Get the best fees',
    findTheFastest: 'Find the fastest',
    trustTheMoreReliable: 'Trust the more reliable',
    showingResults: 'Showing results',
    sortBy: 'Sort by',
    faq: 'Frequently asked questions'
  },
  pt: {
    slogan: 'Compare serviços antes de enviar dinheiro para o exterior. Rápido, seguro e confiável.',
    home: 'INÍCIO',
    about: 'SOBRE',
    youWantToSendMoney: 'Você quer enviar dinheiro...',
    from: 'Origem',
    to: 'Destino',
    amount: 'Valor',
    compare: 'Comparar',
    whyToCompare: 'Por que comparar antes de enviar dinheiro ao exterior?',
    getTheBestFees: 'Busque as menores taxas',
    findTheFastest: 'Encontre o mais rápido',
    trustTheMoreReliable: 'Confie no mais seguro',
    showingResults: 'Mostrando resultados',
    sortBy: 'Classificar por',
    faq: 'Perguntas frequentes'
  }
}

export default strings