import axios from 'axios'

export default {
  getPostsByLanguage (language) {
    return new Promise((resolve, reject) => {
      axios.get(`http://localhost:8080/api/v1/post/language/${language}`)
        .then(result => {
          resolve(result.data)
        })
        .catch(error => {
          console.log(error)
          reject()
        })
    })
  }
}