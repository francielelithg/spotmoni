import axios from 'axios'

export default {
  getAll() {
    return new Promise((resolve, reject) => {
      axios.get(`http://localhost:8080/api/v1/country`)
        .then(result => {
          resolve(result.data)
        })
        .catch(error => {
          console.log(error)
          reject()
        })
    })
  }
}