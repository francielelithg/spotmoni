## spotmoni web

this is spotmoni front-end application. Node.js (=> v12.0.0) is required.

after cloning it, the the following command on terminal to download dependencies:

> npm install

to run application on port 3000, type:

> npm run dev