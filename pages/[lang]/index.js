import withLocale from '../../hocs/withLocale'
import MenuHeader from '../../components/MenuHeader'
import HeadTitle from '../../components/HeadTitle'
import CompareForm from '../../components/CompareForm'
import FAQPreview from '../../components/FAQPreview'
import AdvantageCard from '../../components/AdvantagesCard'
import BlogPreview from '../../components/BlogPreview'
import Countries from '../../components/Countries'
import Footer from '../../components/Footer'
import { Box, Container } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Head from 'next/head'
import React from 'react'

const IndexPage = () => {
  const classes = useStyles()
  return (
    <div>
      <Head>
        <title>spotmoni - compare before sending money abroad</title>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="We compare across 200+ global money transfer providers to show you the best rates" />
      </Head>

      <div className={classes.container}>
        <div className={classes.title}>
          <MenuHeader />
          <Container maxWidth='lg'>
            <Box mb={16}>
              <HeadTitle />
              <CompareForm />
            </Box>
          </Container>
        </div>
      </div>
      <AdvantageCard />
      <FAQPreview />
      <BlogPreview />
      <Countries />
      <Footer />
    </div>
  )
}

const useStyles = makeStyles(() => ({
  container: {
    backgroundImage: `url(${"/images/backgroundHeader.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'bottom',
    backgroundRepeat: 'no-repeat',
    backgroundOrigin: 'bottom',
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  title: {
    backgroundImage: `url(${"/images/world.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'left',
    backgroundRepeat: 'no-repeat',
    backgroundPositionX: '-600px',
    backgroundOrigin: 'left',
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  }
}))

export default withLocale(IndexPage)