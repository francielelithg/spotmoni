import Link from 'next/link'
import withLocale from '../../../hocs/withLocale'
import MainLayout from '../../../layouts/MainLayout'
import Footer from '../../../components/Footer'
import {
  Container,
  Typography
} from '@material-ui/core'
import useTranslation from '../../../hooks/useTanslation'
import React from 'react'

const Wiki = props => {
  const posts = props.posts

  const { locale } = useTranslation()

  return (
    <MainLayout>
      <Container
        maxWidth='md'>
          <Typography component='h4' variant='h5'>
            Wiki
          </Typography>
          {posts.map((post, index) => (
            <div key={index}>
              <Link href="/[lang]/wiki/[id]" as={`/${locale}/wiki/${post.id}`}>
                <a><Typography key={index} variant='body1'>{post.acf.title}</Typography></a>
              </Link>
            </div>
          ))}
      </Container>
      <Footer />
    </MainLayout>
  )
}
/*
Wiki.getInitialProps = async (context) => {
  const props = context.query
  let posts = await cmsService.getAllWikiPosts()
  props.posts = posts
  return props
}
*/

export default withLocale(Wiki)