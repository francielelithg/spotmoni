import withLocale from '../../../hocs/withLocale'
import MainLayout from '../../../layouts/MainLayout'
import Footer from '../../../components/Footer'
import {
  Container,
  Typography
} from '@material-ui/core'
import React, { useState } from 'react'

const WikiPost = props => {
  const [post] = useState(props.post)

  return (
    <MainLayout>
      <Container
        maxWidth='md'>
        <Typography variant='h5'>
          {post.acf.title}
        </Typography>
        <Typography variant='body1'>
          {post.acf.content}
        </Typography>
      </Container>
      <Footer />
    </MainLayout>
  )
}

/*
WikiPost.getInitialProps = async (context) => {
  const props = context.query
  let post = await cmsService.getWikiPostById(props.id)
  props.post = post
  return props
}
*/

export default withLocale(WikiPost)
