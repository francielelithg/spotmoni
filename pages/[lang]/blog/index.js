import withLocale from '../../../hocs/withLocale'
import MenuHeader from '../../../components/MenuHeader'
import Footer from '../../../components/Footer'
import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Container,
  Grid,
  Typography
} from '@material-ui/core'
import { Pagination, Skeleton } from '@material-ui/lab'
import Head from 'next/head'
import React from 'react'

const Blog = () => {
  const classes = useStyles()
  return (
    <div>
      <Head>
        <title>spotmoni - compare before sending money abroad</title>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="We compare across 200+ global money transfer providers to show you the best rates" />
      </Head>

      <div className={classes.container}>
        <MenuHeader />
        <Box mb={24} mt={8}>
          <Typography variant="h2" align="center" color="secondary">Blog</Typography>
        </Box>
      </div>
      <Container maxWidth='lg'>
        <Box my={12}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={4}>
              <Box mx={3}>
                <Box my={4}>
                  <Skeleton variant="rect" width='100%' height="476px" />
                </Box>
                <Typography variant='h6'>
                  How to Minimize Your Forex Fees
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box mx={3}>
                <Box my={4}>
                  <Skeleton variant="rect" width='100%' height="476px" />
                </Box>
                <Typography variant='h6'>
                  Spotmoni.com’s Price Alert Feature Can Save You a Ton
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box mx={3}>
                <Box my={4}>
                  <Skeleton variant="rect" width='100%' height="476px" />
                </Box>
                <Typography variant='h6'>
                  How to Send Cheaper and Faster
                </Typography>
              </Box>
            </Grid>
          </Grid>
          <Box display="flex" justifyContent="center" mt={12}>
            <Pagination count={10} shape="rounded" size="large" />
          </Box>
        </Box>
      </Container>
      <Footer />
    </div>
  )
}

const useStyles = makeStyles(() => ({
  container: {
    backgroundImage: `url(${"/images/backgroundCompare.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'bottom',
    backgroundRepeat: 'no-repeat',
    backgroundOrigin: 'bottom',
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
}))

export default withLocale(Blog)
