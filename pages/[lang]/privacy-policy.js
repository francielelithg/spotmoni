import withLocale from '../../hocs/withLocale'
import MenuHeader from '../../components/MenuHeader'
import Footer from '../../components/Footer'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Container, Typography } from '@material-ui/core'
import Head from 'next/head'
import React from 'react'

const PrivacyPolicy = () => {
  const classes = useStyles()
  return (
    <div>
      <Head>
        <title>spotmoni - compare before sending money abroad</title>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="We compare across 200+ global money transfer providers to show you the best rates" />
      </Head>

      <div className={classes.container}>
        <MenuHeader />
        <Box mb={24} mt={8}>
          <Typography variant="h2" align="center" color="secondary">Privacy policy</Typography>
        </Box>
      </div>
      <Container maxWidth='md'>
        <Box my={12} textAlign="justify">
          <Typography variant='h6'>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo viverra maecenas accumsan lacus vel facilisis. Diam maecenas ultricies mi eget mauris pharetra et ultrices neque. Tellus elementum sagittis vitae et leo duis ut diam quam. Feugiat nibh sed pulvinar proin gravida. Pretium aenean pharetra magna ac placerat vestibulum lectus. Amet commodo nulla facilisi nullam vehicula ipsum a. Vitae nunc sed velit dignissim sodales ut. Sed augue lacus viverra vitae congue eu. Lectus arcu bibendum at varius. Enim praesent elementum facilisis leo vel fringilla est. Eu lobortis elementum nibh tellus molestie nunc. Rhoncus urna neque viverra justo nec ultrices dui. Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Ut pharetra sit amet aliquam. Egestas purus viverra accumsan in nisl nisi scelerisque. Augue neque gravida in fermentum et sollicitudin. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor.
            </p>
            <p>Lacus suspendisse faucibus interdum posuere lorem ipsum dolor. Diam ut venenatis tellus in metus vulputate eu scelerisque felis. In metus vulputate eu scelerisque felis. Vitae tempus quam pellentesque nec nam aliquam sem. Vivamus at augue eget arcu dictum varius duis at consectetur. Senectus et netus et malesuada fames ac turpis. Neque ornare aenean euismod elementum nisi. Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Rhoncus dolor purus non enim. Commodo ullamcorper a lacus vestibulum sed. At consectetur lorem donec massa sapien faucibus et. Dolor sit amet consectetur adipiscing elit duis tristique sollicitudin. Tellus in metus vulputate eu. Arcu dictum varius duis at consectetur lorem. In nulla posuere sollicitudin aliquam ultrices sagittis orci a. Faucibus nisl tincidunt eget nullam non nisi est sit. Non odio euismod lacinia at quis risus sed vulputate. Fames ac turpis egestas maecenas pharetra.
            </p>
            <p>Ut etiam sit amet nisl purus in mollis. In aliquam sem fringilla ut morbi. Ante in nibh mauris cursus mattis molestie. Lorem sed risus ultricies tristique nulla aliquet enim tortor. Quis blandit turpis cursus in hac. Euismod lacinia at quis risus. Faucibus vitae aliquet nec ullamcorper sit. Tortor consequat id porta nibh venenatis cras sed felis. Mi quis hendrerit dolor magna. Elit pellentesque habitant morbi tristique senectus.
            </p>
            <p>In tellus integer feugiat scelerisque. Pharetra vel turpis nunc eget lorem dolor sed viverra. Mauris nunc congue nisi vitae suscipit. Sit amet porttitor eget dolor. Massa ultricies mi quis hendrerit dolor magna eget est lorem. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit duis. Adipiscing elit ut aliquam purus sit amet luctus venenatis lectus. Massa enim nec dui nunc mattis. Quam elementum pulvinar etiam non quam lacus. Feugiat in ante metus dictum at tempor. Non nisi est sit amet.
            </p>
            <p>Mi ipsum faucibus vitae aliquet nec ullamcorper sit amet. In hac habitasse platea dictumst vestibulum rhoncus est. Nisl vel pretium lectus quam id leo in vitae turpis. Lectus sit amet est placerat in egestas erat imperdiet sed. Purus gravida quis blandit turpis cursus in hac. Semper quis lectus nulla at. Duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat. Pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada. Adipiscing at in tellus integer feugiat scelerisque varius morbi. Eget dolor morbi non arcu risus. Tempus iaculis urna id volutpat.
            </p>
          </Typography>
        </Box>
      </Container>
      <Footer />
    </div>
  )
}

const useStyles = makeStyles(() => ({
  container: {
    backgroundImage: `url(${"/images/backgroundCompare.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'bottom',
    backgroundRepeat: 'no-repeat',
    backgroundOrigin: 'bottom',
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
}))

export default withLocale(PrivacyPolicy)
