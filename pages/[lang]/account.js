import withLocale from '../../hocs/withLocale'
import withAuth from '../../hocs/withAuth'
import AccountLayout from '../../layouts/AccountLayout'
import {
  Container
} from '@material-ui/core'
import { compose } from 'recompose'
import React from 'react'

const Account = () => {
  return (
    <AccountLayout>
      <Container
        maxWidth='md'>
        
      </Container>
    </AccountLayout>
  )
}

export default compose(withLocale, withAuth)(Account)