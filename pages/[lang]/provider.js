import withLocale from '../../hocs/withLocale'
import MainLayout from '../../layouts/MainLayout'
import Footer from '../../components/Footer'
import {
  Box,
  Chip,
  Container,
  Grid,
  Typography
} from '@material-ui/core'
import { Rating } from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'
import React from 'react'

const About = () => {
  const classes = useStyles()
  return (
    <MainLayout>
      <Container
        maxWidth='md'>
        <Typography variant='h3'>
          REMITBEE
        </Typography>
        <Rating
          name="simple-controlled"
          value={4}
          size="large"
        />
        <Box my={4}>
          <Typography align='justify'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Morbi tincidunt augue interdum velit euismod in pellentesque massa. Risus at ultrices mi tempus imperdiet. Eu sem integer vitae justo eget magna.
        </Typography>
        </Box>

        <Box my={4}>
          <Typography variant='h6'>
            Popular currencies exchanged through Remitbee
          </Typography>
          <div className={classes.root}>
            <Chip size="large" label="CAD" />
            <Chip size="large" label="USD" />
            <Chip size="large" label="INR" />
            <Chip size="large" label="EUR" />
            <Chip size="large" label="BRL" />
            <Chip size="large" label="GBP" />
            <Chip size="large" label="JPY" />
          </div>
        </Box>

        <Typography variant='h6'>
          Reviews (34)
        </Typography>

        <Box my={4}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={10}>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Morbi tincidunt augue interdum velit euismod in pellentesque massa. Risus at ultrices mi tempus imperdiet. Eu sem integer vitae justo eget magna.
            </Typography>
              <Box fontStyle="italic">Anonymous user</Box>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Rating
                name="simple-controlled"
                value={4}
              />
            </Grid>
          </Grid>
        </Box>

        <Box my={4}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={10}>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Morbi tincidunt augue interdum velit euismod in pellentesque massa. Risus at ultrices mi tempus imperdiet. Eu sem integer vitae justo eget magna.
            </Typography>
              <Box fontStyle="italic">Anonymous user</Box>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Rating
                name="simple-controlled"
                value={5}
              />
            </Grid>
          </Grid>
        </Box>

        <Box my={4}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={10}>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Morbi tincidunt augue interdum velit euismod in pellentesque massa. Risus at ultrices mi tempus imperdiet. Eu sem integer vitae justo eget magna.
            </Typography>
              <Box fontStyle="italic">Anonymous user</Box>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Rating
                name="simple-controlled"
                value={5}
              />
            </Grid>
          </Grid>
        </Box>

        <Box display="flex" justifyContent="center" mt={2} mb={10}>
          <Typography variant="button">
            See more
          </Typography>
        </Box>

      </Container>

      <Footer />
    </MainLayout>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}))

export default withLocale(About)