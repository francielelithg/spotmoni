import withLocale from '../../../../../hocs/withLocale'
import MenuHeader from '../../../../../components/MenuHeader'
import CompareForm from '../../../../../components/CompareForm'
import ResultCard from '../../../../../components/ResultCard'
import RelatedCountryText from '../../../../../components/RelatedCountryText'
import FAQPreview from '../../../../../components/FAQPreview'
import Footer from '../../../../../components/Footer'
import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Grid,
  InputBase,
  Paper,
  Typography,
} from '@material-ui/core'
import Logo from '../../../../../components/Logo'
import { useRouter } from 'next/router'
import Head from 'next/head'
import React, { useState, useEffect } from 'react'

const Compare = () => {
  const router = useRouter()
  const classes = useStyles()
  const [data] = useState(router.query)
  const s_country = data.from
  const r_country = data.to
  const amount = data.amount

  return (
    <div>
      <Head>
        <title>spotmoni - compare before sending money abroad</title>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="We compare across 200+ global money transfer providers to show you the best rates" />
      </Head>

      <div className={classes.container}>
        <MenuHeader />
        <Box pb={18}>
          <Container maxWidth='lg'>
            <CompareForm {...data} />
          </Container>
        </Box>
      </div>

      <Results data={data}/>

      <Container maxWidth='md'>
        <Box mb={8}>
          <Paper elevation={0} className={classes.body}>
            <Box p={3}>
              <Grid container spacing={0}>
                <Grid item xs={12} md={5}>
                  <Box display="flex" alignItems="center" css={{ height: '100%' }}>
                    <Typography variant='h6'>
                      Be notified of a price drop.
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={12} md={3} className={classes.input}>
                  <InputBase
                    required
                    fullWidth
                    placeholder='E-mail'
                    name="to"
                    classes={{ root: classes.textfield }}
                  />
                </Grid>
                <Grid item xs={12} md={4} className={classes.input}>
                  <Box display="flex">
                    <Box flexGrow={1}>
                      <InputBase
                        required
                        fullWidth
                        placeholder='Amount'
                        name="amount"
                        type='number'
                        classes={{ root: classes.textfield }}
                      />
                    </Box>
                    <Box display="flex" alignItems="center" css={{ height: '100%' }}>
                      <Button
                        fullWidth
                        disableElevation
                        size='large'
                        color='primary'
                        type='submit'
                        className={classes.button}>
                        ok
                      </Button>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Paper>
        </Box>
      </Container>

      <RelatedCountryText />

      <FAQPreview />

      <Footer />
    </div>
  )
}

class Results extends React.Component{
  
  constructor(props){
    super(props); 
    console.log(this.props)
    this.state = {
      com_list: []

    }
  }
  
  componentDidMount(){
    fetch("http://127.0.0.1:5000/api/input",{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        d_country: String(this.props.data.to), 
        s_country: String(this.props.data.from), 
        amount: this.props.data.amount
      })
    }).then(res => res.json()).then(data => this.setState(data));
  }
  render(){
    {if (this.state.com_list.length != 0){
      return (
      
        <div>
          {/* {this.state.com_list.map((item, i)=> (
            <h1 key = {i}>{item}</h1>
          ))} */}
          {console.log(this.state.com_list)}
          <ApiFetch com_list = {this.state.com_list} s_country = {this.props.data.from} r_country = {this.props.data.to} amount={this.props.data.amount}/>
          
        </div>
        
      )

    }}
    return(
      <div>
        <Box display="flex" justifyContent="center" my={16}>
          <CircularProgress />
        </Box>
      </div>
    )
    
  }
}

function ApiFetch(props){
  // const [com_list, set_cm] = useState(props.com_list[0])
  // console.log(props.com_list[0])
  const [com_list, set_cm] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
  const [results, set_results] = useState([])
  console.log(props.r_country)
  console.log(props.s_country)
  console.log(props.amount)

  
  useEffect(() => {
    const r_list = [];
    
    // let mylist = [...results];
    const myrequest = com_list.map((com)=>(
      api_fetch(com)
    ))

    async function api_fetch(company){
      
    //   const response = await fetch('http://127.0.0.1:5000/rate/' + company + "?o_country=" + props.s_country +"&d_country=" + 
    //   props.r_country + "&amount=" + props.amount, {method: 'GET'})
    // var myjson = await response.json();
    try{
      const response = await fetch('http://127.0.0.1:5000/rate/' + company + "?o_country=" + props.s_country +"&d_country=" + 
        props.r_country + "&amount=" + props.amount, {method: 'GET'})
      var myjson = await response.json();

    } catch{
      myjson = {"company": "Error", "rate": "0.00"}
    }

        
    
    return r_list.push(myjson)

      
    }
    Promise.all(myrequest).then(()=>{
      function sortByValue(){
        return function(a,b){
          console.log(parseFloat(a['rate']))
          console.log(parseFloat(b['rate']))

          if(parseFloat(a['rate']) > parseFloat(b['rate'])){
            console.log('-1')
            return -1;
          }
    
            
          else if (parseFloat(a['rate']) < parseFloat(b['rate'])){
            console.log('1')
            return 1; 
          }
            
          return 0; 
        }
      }
      r_list.sort(sortByValue());
      // var size = 10; 
      set_results(r_list)
    })
    
  }, [])

  if ((com_list.length != 0) || (results != []) ){
    return(
      <div>
        {console.log(props.r_country)}
          {console.log(com_list)}
          {console.log(results)}
          {results.slice(0, 11).map((x, index)=>
            // <p key = {index}>{x.company} {x.rate}</p>
            <ResultCard key = {index} input = {props.amount} output = {x.rate} company = {x.company} from ={props.s_country} to = {props.r_country}/>
          )}
          
      </div>
    )
  }
  return(
    <div>
      <Box display="flex" justifyContent="center" my={16}>
          <CircularProgress />
      </Box>
    </div>
  )
    
    // {com_list.map((com)=> (
    //   <p>{com}</p>
    // ))}
    // // {country_info.map((country) => (
    //   <MenuItem value = {country[0]} key = {country[0]}>{country[1]}</MenuItem>
    //   ))}
  
}


const Compare2 = () => {
  

  if (data) {
    useEffect(() => {
      setTimeout(function () {
        setResults([
          { name: 'WorldRemit', value: 124.00 },
          { name: 'Western Union', value: 127.00}
        ])
      }, 2500)
    }, [])
  }

  return (
    <MainLayout>
      <div className={classes.container}>
        <MenuHeader />
        <Box pb={18}>
          <Container maxWidth='lg'>
            <Logo />
            <CompareForm {...data} />
          </Container>
        </Box>
      </div>

      {!results && ( 
        <Box display="flex" justifyContent="center" my={16}>
          <CircularProgress />
        </Box>
      )}

      {results && results.map((result, index) => (
        <ResultCard key={index} />
      ))}

      <Container maxWidth='md'>
        <Box mb={8}>
          <Paper elevation={0} className={classes.body}>
            <Box p={3}>
              <Grid container spacing={0}>
                <Grid item xs={12} md={5}>
                  <Box display="flex" alignItems="center" css={{ height: '100%' }}>
                    <Typography variant='h6'>
                      Be notified of a price drop.
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={12} md={3} className={classes.input}>
                  <InputBase
                    required
                    fullWidth
                    placeholder='E-mail'
                    name="to"
                    classes={{ root: classes.textfield }}
                  />
                </Grid>
                <Grid item xs={12} md={4} className={classes.input}>
                  <Box display="flex">
                    <Box flexGrow={1}>
                      <InputBase
                        required
                        fullWidth
                        placeholder='Amount'
                        name="amount"
                        type='number'
                        classes={{ root: classes.textfield }}
                      />
                    </Box>
                    <Box display="flex" alignItems="center" css={{ height: '100%' }}>
                      <Button
                        fullWidth
                        disableElevation
                        size='large'
                        color='primary'
                        type='submit'
                        className={classes.button}>
                        ok
                      </Button>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Paper>
        </Box>
      </Container>

      <RelatedCountryText />

      <FAQPreview />

      <Footer />
    </MainLayout>
  )
}

/*
Compare.getInitialProps = async (context) => {
  const props = context.query
  const response = await fetch(`https://api.exchangeratesapi.io/history?start_at=2020-05-12&end_at=2020-07-12&base=${props.from}&symbols=${props.to}`)
  props.exchangeRates = await response.json()
  return props
}
*/

const useStyles = makeStyles((theme) => ({
  resultsTitle: {
    padding: theme.spacing(5, 0, 0, 0)
  },
  container: {
    backgroundImage: `url(${"/images/backgroundCompare.svg"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'bottom',
    backgroundRepeat: 'no-repeat',
    backgroundOrigin: 'bottom',
    flexDirection: 'column',
    width: '100%'
  },
  body: {
    background: theme.palette.secondary.light,
  },
  textfield: {
    background: theme.palette.secondary.main,
    height: '100%',
    padding: theme.spacing(0, 1.5),
    border: 'solid 2px rgba(86, 91, 138, 0.12)',
    borderRadius: '4px',
  },
  input: {
    [theme.breakpoints.up('md')]: {
      paddingLeft: theme.spacing(1),
    },
    [theme.breakpoints.down('md')]: {
      width: '41.6px',
      paddingTop: theme.spacing(1),
    },
  },
  button: {
    marginLeft: theme.spacing(1),
  }
}))

export default withLocale(Compare)
