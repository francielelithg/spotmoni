import { Provider } from 'next-auth/client'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import languageService from '../services/language'
import { wrapper } from '../store'
import React from 'react'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#565b8a'
    },
    secondary: {
      light: '#f2f2f6',
      main: '#ffffff',
      dark: '#e9e9f0'
    },
    featured: {
      main: '#5381ae'
    }
  },
  typography: {
    fontFamily: ["Product Sans", "Roboto"].join(","),
    fontWeightRegular: 500
  }
})

const App = ({ Component, pageProps }) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Provider options={{ site: process.env.SITE }} session={pageProps.session}>
          <Component {...pageProps} />
      </Provider>
    </MuiThemeProvider>
  )
}

App.getInitialProps = async ({ Component, ctx }) => {
  const languages = await languageService.getAll()
  const codes = languages.map(value => value.code)

  ctx.store.dispatch({
    type: 'TICK',
    languages: languages,
    codes: codes
  })

  return {
    pageProps: {
      ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
    }
  }
}

export default wrapper.withRedux(App)